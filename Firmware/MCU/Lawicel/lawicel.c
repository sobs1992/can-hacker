#include "lawicel.h"
#include <string.h>
#include "..\SEGGER_RTT.h"

uint32_t can_speed, can_status, timestamp, usb_mutex;
uint32_t filter_mask, filter_code;

void Lawicel_Init(){
    Leds_Init();
    USART_Init();
    USB_Init();
    USB_CDC_SetParameter(115200, 8, CDC_PARITY_NONE, CDC_STOP_1);
    filter_mask = 0;
    filter_code = 0;
    CAN_SetFilter(0, CAN_FILTER_MASK, CAN_EXTENDED, filter_code, filter_mask);
    CAN_SetFilter(1, CAN_FILTER_MASK, CAN_STANDART, filter_code, filter_mask);
}

uint8_t CAN_GetStatus(){
    return can_status;
}

void USB_ClearMutex(){
    usb_mutex = 0;
}

void Lawicel_Send(uint8_t *message, uint8_t source){
    uint16_t cnt;
#ifdef DEBUG_MSG
    SEGGER_RTT_WriteString(0, message);
    SEGGER_RTT_WriteString(0, "\n");
#endif
    if ((source == SOURCE_USART) || (source == SOURCE_ALL)){
        USART_Write(message);
    }  
    if ((source == SOURCE_USB) || (source == SOURCE_ALL)){
        if (USB_GetState() == USB_CONFIGURE_STATE){
            while (usb_mutex){
                cnt++;
                if (cnt >= 10000) {
                    break;
                }
            }
            EP_Write(1, message, strlen(message));
            usb_mutex = 1;
        }
    }
}

void Lawicel_Read(uint8_t *recv_buf, uint8_t source){
    uint8_t temp[10], i;
    _can_message message;
    SEGGER_RTT_WriteString(0, recv_buf);
    SEGGER_RTT_WriteString(0, "\n");
    switch (recv_buf[0]){
        case 'V': Lawicel_Send("V0110", source);   break;
        case 'v': Lawicel_Send("vLawicel", source);  break;
        case 'N': Lawicel_Send("N0001", source);   break;
        case 'S': 
            can_speed = CAN_GetSpeed(recv_buf[1]);
            break;
        case 's':
            can_speed = strtoul(&recv_buf[1], NULL, 16) | (2 << 24);
            break;
        case 'L': 
            CAN_Init(can_speed, MODE_LISTEN_ONLY); 
            LED_Connect_Blink_On();
            can_status = CAN_STATUS_CONNECTED_LISTEN_ONLY;
#ifdef DEBUG_MSG
            SEGGER_RTT_WriteString(0, "CAN_Init_Listen_Only\n");
#endif
            break;
        case 'O': 
            CAN_Init(can_speed, MODE_NORMAL); 
            LED_Connect_On();
            can_status = CAN_STATUS_CONNECTED;
#ifdef DEBUG_MSG
            SEGGER_RTT_WriteString(0, "CAN_Init\n");
#endif
            break;
        case 'C': 
            can_status = CAN_STATUS_CLOSE;
            LED_Connect_Blink_Off();
            CAN_DeInit();
            break;
        case 'M':
            recv_buf[9] = 0;
            filter_code = strtoul(&recv_buf[1], NULL, 16);
            CAN_SetFilter(0, CAN_FILTER_MASK, CAN_EXTENDED, filter_code, filter_mask);
            CAN_SetFilter(1, CAN_FILTER_MASK, CAN_STANDART, filter_code, filter_mask);
            break;
        case 'm':
            recv_buf[9] = 0;
            filter_mask = ~strtoul(&recv_buf[1], NULL, 16);
            CAN_SetFilter(0, CAN_FILTER_MASK, CAN_EXTENDED, filter_code, filter_mask);
            CAN_SetFilter(1, CAN_FILTER_MASK, CAN_STANDART, filter_code, filter_mask);
            break;
        case 'r':
        case 't':
            message.type = CAN_STANDART;
            memcpy(temp, &recv_buf[1], 3);
            temp[3] = 0;
            message.id = strtoul(temp, NULL, 16);
            message.DLC = recv_buf[4] - '0';
            message.RTR = (recv_buf[0] == 't') ? 0 : 1;
            for (i = 0; i < message.DLC; i++){
                memcpy(temp, &recv_buf[5 + i*2], 2);
                temp[2] = 0;
                message.data[i] = strtoul(temp, NULL, 16);
            }
            if (CAN_GetStatus() == CAN_STATUS_CONNECTED) CAN_SetTxMessage(message);
            break;
        case 'R':
        case 'T':
            message.type = CAN_EXTENDED;
            memcpy(temp, &recv_buf[1], 8);
            temp[8] = 0;
            message.id = strtoul(temp, NULL, 16);
            message.DLC = recv_buf[9] - '0';
            message.RTR = (recv_buf[0] == 'T') ? 0 : 1;
            for (i = 0; i < message.DLC; i++){
                memcpy(temp, &recv_buf[10 + i*2], 2);
                temp[2] = 0;
                message.data[i] = strtoul(temp, NULL, 16);
            }
            if (CAN_GetStatus() == CAN_STATUS_CONNECTED) CAN_SetTxMessage(message);
            break;
        case 'Z':
            if (recv_buf[1] == '0') timestamp = 0;
            else if (recv_buf[1] == '1') timestamp = 1;
            break;
        default: 
            
            break;
    }
    Lawicel_Send("\r", source);
}

void Lawicel_SendMessage(_can_message message, uint8_t source){
    uint8_t temp[10], send_message[35];
    uint8_t len, i, j, temp_len;
    uint32_t cnt = 0;

    if (message.type == CAN_STANDART){
        if (message.RTR) send_message[0] = 'r'; else send_message[0] = 't';
        itoa(message.id, temp, 16);
        j = 3;
    } else {
        if (message.RTR) send_message[0] = 'R'; else send_message[0] = 'T';
        itoa(message.id, temp, 16);
        j = 8;
    }
    
    temp_len = strlen(temp);
    len = j - temp_len;
    if (len > j) len = 0;
    for (i = 0; i < len; i++){
        send_message[i + 1] = '0';
    }
    strcpy(&send_message[++i], temp);
    i += temp_len - 1;
    itoa(message.DLC, temp, 16);
    send_message[++i] = temp[0];
    for (j = 0; j < message.DLC; j++){
        itoa(message.data[j], temp, 16);
        if (message.data[j] < 0x10){
            send_message[++i] = '0';
            send_message[++i] = temp[0];
        } else {
            send_message[++i] = temp[0];
            send_message[++i] = temp[1];
        }
    }
    if (timestamp){
        itoa(message.time, temp, 16);
        temp_len = strlen(temp);
        len = 4 - temp_len;
        if (len > 4) len = 0;
        for (j = 0; j < len; j++){
            send_message[++i] = '0';
        }
        strcpy(&send_message[++i], temp);
        i += temp_len - 1;
    }
    send_message[++i] = '\r';
    send_message[++i] = 0;
    Lawicel_Send(send_message, source);
}
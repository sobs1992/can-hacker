#pragma once

#include <stm32f0xx.h>
#include "..\CAN\can.h"
#include "..\LEDS\leds.h"
#include "..\USART\usart.h"
#include "..\usb_lib\usb_lib.h"
#include "..\main.h"

#define CAN_STATUS_CLOSE                    0
#define CAN_STATUS_CONNECTED                1
#define CAN_STATUS_CONNECTED_LISTEN_ONLY    2

#define SOURCE_USB                          1
#define SOURCE_ALL                          2

void Lawicel_Init();
void USB_ClearMutex();
void Lawicel_Send(uint8_t *message, uint8_t source);
void Lawicel_Read(uint8_t *recv_buf, uint8_t source);
uint8_t CAN_GetStatus();
void Lawicel_SendMessage(_can_message message, uint8_t source);
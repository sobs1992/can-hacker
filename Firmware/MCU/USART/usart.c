#include "usart.h"
#include <string.h>

uint8_t send_buf[USART_SIZE_SEND_BUF];
uint8_t recv_buf[USART_SIZE_RECV_BUF];

uint8_t recv_buf_cnt = 0;
uint8_t usart_mutex;

void USART_Write(uint8_t *buf){
    uint32_t cnt = 0;
    uint8_t len = strlen(buf);
    memcpy(send_buf, buf, len);
    while (usart_mutex){
        cnt++;
        if (cnt >= 10000) {
            break;
        }
    }

    DMA1_Channel2 -> CCR &= ~(DMA_CCR_EN);
    DMA1_Channel2 -> CNDTR = len;
    DMA1_Channel2 -> CCR |= DMA_CCR_EN;
    usart_mutex = 1;
}

uint8_t USART_GetReady(){
    return usart_mutex;
}

void USART1_IRQHandler(){
    uint8_t byte;
    if (USART1 -> ISR & USART_ISR_RXNE){
        byte = USART1 -> RDR;
	recv_buf[recv_buf_cnt] = byte;
        if (byte == '\r'){
            recv_buf[recv_buf_cnt] = 0;
            Lawicel_Read(recv_buf, SOURCE_USART);
            recv_buf_cnt = 0;
            return;
        }
        if (recv_buf_cnt < (USART_SIZE_RECV_BUF - 1)) recv_buf_cnt++;
	else recv_buf_cnt = 0;
    }
}

void DMA1_Ch2_3_DMA2_Ch1_2_IRQHandler(){
    DMA1 -> IFCR = DMA_IFCR_CTCIF2;
    usart_mutex = 0;
}

void USART_Init(){
    RCC -> AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_DMA1EN;
    RCC -> APB2ENR |= RCC_APB2ENR_USART1EN;

    GPIOA -> MODER |= GPIO_MODER_MODER9_1 | GPIO_MODER_MODER10_1;
    GPIOA -> OSPEEDR = 0xFFFFFFFF;
    GPIOA -> AFR[1] = (1 << 8) | (1 << 4);

    USART1 -> CR1 = USART_CR1_TE | USART_CR1_RE | USART_CR1_RXNEIE;
    USART1 -> CR3 = USART_CR3_DEM | USART_CR3_DMAT | USART_CR3_OVRDIS;
    USART1 -> BRR = 48000000 / 115200;
    USART1 -> CR1 |= USART_CR1_UE;

    DMA1_Channel2 -> CMAR = (uint32_t)send_buf;
    DMA1_Channel2 -> CPAR = (uint32_t)&USART1 -> TDR;
    DMA1_Channel2 -> CCR = DMA_CCR_PL | DMA_CCR_MINC | DMA_CCR_DIR | DMA_CCR_TCIE;
    
    NVIC_EnableIRQ(USART1_IRQn);
    NVIC_EnableIRQ(DMA1_Ch2_3_DMA2_Ch1_2_IRQn);
}


#pragma once

#include <stm32f0xx.h>
#include "..\CAN\can.h"

#define USART_SIZE_SEND_BUF  64
#define USART_SIZE_RECV_BUF  64

#define SOURCE_USART            0

extern void Lawicel_Read(uint8_t *recv_buf, uint8_t source);

void USART_Write(uint8_t *buf);
void USART_Init();
//void USART_SendCanMessage(_can_message message);
uint8_t USART_GetReady();
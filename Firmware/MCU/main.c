#include <stm32f0xx.h>
#include "SEGGER_RTT.h"
#include "CAN\can.h"
#include "Lawicel\lawicel.h"
#include "main.h"

#define USB_SIZE_RECV_BUF  64

uint8_t usb_recv_buf[USB_SIZE_RECV_BUF], usb_recv_buf_cnt;

void SetPll48(){
    RCC -> CR |= RCC_CR_HSEON;
    while (!(RCC -> CR & RCC_CR_HSERDY));
    RCC -> CFGR = RCC_CFGR_PLLMUL3 | RCC_CFGR_PLLSRC_HSE_PREDIV;
    FLASH -> ACR = FLASH_ACR_PRFTBE | FLASH_ACR_LATENCY;
    RCC -> CR |= RCC_CR_PLLON;
    while (!(RCC -> CR & RCC_CR_PLLRDY));
    RCC -> CFGR |= RCC_CFGR_SW_PLL;
}

void EP_UserInit(){
    EP_Init(1, EP_TYPE_BULK, 384, 512);
    EP_Init(2, EP_TYPE_INTERRUPT, 800, 1000);
}

void EP_Handler(uint8_t ep_num){
    uint8_t buf[20], txt[10], i, byte;
    if (ep_num == 1){
        if (endpoints[ep_num].rx_flag){
            EP_Read_NoConfirm(ep_num, buf);
#ifdef DEBUG_MSG
            SEGGER_RTT_WriteString(0, "RX ");
#endif
            for (i = 0; i < endpoints[ep_num].rx_cnt; i++){
#ifdef DEBUG_MSG
               itoa(buf[i], txt, 16);
                SEGGER_RTT_WriteString(0, txt);
                SEGGER_RTT_WriteString(0, " ");
#endif
                usb_recv_buf[usb_recv_buf_cnt] = buf[i];
                if (buf[i] == '\r'){
                    usb_recv_buf[usb_recv_buf_cnt] = 0;
                    Lawicel_Read(usb_recv_buf, SOURCE_USB);
                    usb_recv_buf_cnt = 0;
                    break;
                } else {
                    if (usb_recv_buf_cnt < (USB_SIZE_RECV_BUF - 1)) usb_recv_buf_cnt++;
                    else usb_recv_buf_cnt = 0;
                }
            }
#ifdef DEBUG_MSG
            SEGGER_RTT_WriteString(0, "\n");
#endif
            EP_Confirm_Read(ep_num);
            endpoints[ep_num].rx_flag = 0;
           // endpoints[ep_num].status = SET_VALID_RX(endpoints[ep_num].status);
           // endpoints[ep_num].status = KEEP_STAT_TX(endpoints[ep_num].status);    
        }
        if (endpoints[ep_num].tx_flag){
            endpoints[ep_num].tx_flag = 0;
            USB_ClearMutex();
            EP_Confirm_Read(ep_num);
#ifdef DEBUG_MSG
            SEGGER_RTT_WriteString(0, "TX OK\n");
#endif
         //   endpoints[ep_num].status = SET_VALID_RX(endpoints[ep_num].status);
         //   endpoints[ep_num].status = KEEP_STAT_TX(endpoints[ep_num].status);  
        }
    }
}

void main(void){
    _can_message message;
    SetPll48();
#ifdef DEBUG_MSG
    SEGGER_RTT_Init();
#endif
    SystemCoreClockUpdate();
    CAN_GPIO_Init();
    Lawicel_Init();
#ifdef DEBUG_MSG
    SEGGER_RTT_WriteString(0, "START\r\n");
#endif

    IWDG -> KR = 0x0000CCCC;
    IWDG -> KR = 0x00005555;
    IWDG -> PR = 5;
    IWDG -> RLR = 0xFF;
    while (IWDG -> SR);
    IWDG -> KR = 0x0000AAAA;
    
    while(1){
        IWDG -> KR = 0x0000AAAA;
        Enumerate(0);
        EP_Handler(1);
        if ((CAN_TxIsEmpty() == 0)){
            message = CAN_GetTxMessage();
            CAN_Transmit(message);
        }
        if ((CAN_RxIsEmpty() == 0) && (USART_GetReady() == 0)){
            message = CAN_GetRxMessage();
            Lawicel_SendMessage(message, SOURCE_ALL);
        }
    }
}

#include "can.h"

#define CAN_SPEED_10K   (2 << 24 | 4 << 20 | 9 << 16 | 299 << 0)
#define CAN_SPEED_20K   (2 << 24 | 4 << 20 | 9 << 16 | 149 << 0)
#define CAN_SPEED_50K   (2 << 24 | 4 << 20 | 9 << 16 | 59 << 0)
#define CAN_SPEED_100K  (2 << 24 | 4 << 20 | 8 << 16 | 31 << 0)
#define CAN_SPEED_125K  (2 << 24 | 4 << 20 | 9 << 16 | 23 << 0)
#define CAN_SPEED_250K  (2 << 24 | 4 << 20 | 9 << 16 | 11 << 0)
#define CAN_SPEED_500K  (2 << 24 | 4 << 20 | 9 << 16 | 5 << 0)
#define CAN_SPEED_800K  (2 << 24 | 6 << 20 | 11 << 16 | 2 << 0)
#define CAN_SPEED_1M    (2 << 24 | 5 << 20 | 8 << 16 | 2 << 0)

_can_message can_tx_messages[CAN_TXBUF_SIZE];
_can_message can_rx_messages[CAN_RXBUF_SIZE];

uint8_t countTxBuf, headTxBuf, tailTxBuf;
uint8_t countRxBuf, headRxBuf, tailRxBuf;

void CAN_GPIO_Init(){
    RCC -> AHBENR |= RCC_AHBENR_GPIOBEN;
    RCC -> APB1ENR |= RCC_APB1ENR_CANEN;
    RCC -> APB2ENR |= RCC_APB2ENR_SYSCFGEN | RCC_APB2ENR_TIM1EN;

    GPIOB -> MODER |= GPIO_MODER_MODER8_1 | GPIO_MODER_MODER9_1;
    GPIOB -> OSPEEDR |= GPIO_OSPEEDER_OSPEEDR8 | GPIO_OSPEEDER_OSPEEDR9;
    GPIOB -> AFR[1] |= (4 << 4) | (4 << 0);

    TIM1 -> PSC = 48000 - 1;
    TIM1 -> ARR = 59999;
    TIM1 -> CR1 = TIM_CR1_CEN;

    countTxBuf = 0;
    countRxBuf = 0;
}

_can_message CAN_Receive(){
    uint8_t i;
    _can_message message;
    message.time = TIM1 -> CNT;
    message.type = (CAN -> sFIFOMailBox[0].RIR & CAN_RI0R_IDE) ? CAN_EXTENDED : CAN_STANDART;
    if (message.type == CAN_EXTENDED){
        message.id = CAN -> sFIFOMailBox[0].RIR >> 3;
    } else {
        message.id = CAN -> sFIFOMailBox[0].RIR >> 21;
    }
    message.RTR = CAN -> sFIFOMailBox[0].RIR & CAN_RI0R_RTR;
    message.DLC = CAN -> sFIFOMailBox[0].RDTR & CAN_RDT0R_DLC;
    for (i = 0; (i < message.DLC) && (i < 4); i++){
        message.data[i] = (CAN -> sFIFOMailBox[0].RDLR >> (8 * i)) & 0xFF;
    }
    for (i = 4; (i < message.DLC) && (i < 8); i++){
        message.data[i] = (CAN -> sFIFOMailBox[0].RDHR >> (8 * (i - 4))) & 0xFF;
    }
    CAN -> RF0R |= CAN_RF0R_RFOM0;
    return message;
}

void CEC_CAN_IRQHandler(){
    if (CAN -> RF0R & CAN_RF0R_FMP0){
        CAN_SetRxMessage(CAN_Receive());
        LED_RX_Blink();
    }
}

void CAN_SetFilter(uint8_t num, uint8_t filter_mode, uint8_t id_type, uint32_t ID1, uint32_t ID2_MASK){
    CAN -> FMR |= CAN_FMR_FINIT;
    CAN -> FA1R &= ~(1 << num);      //DEACTIVE
    if (filter_mode == CAN_FILTER_MASK){
        CAN -> FM1R &= ~(1 << num);
    } else {
        CAN -> FM1R |= (1 << num);
    }
    CAN -> FS1R |= (1 << num);       //32bit
    CAN -> FFA1R &= ~(1 << num);    //FIFO 0

    if (id_type == CAN_EXTENDED){
        CAN -> sFilterRegister[num].FR1 = (ID1 << 3) | 0x04; 
        CAN -> sFilterRegister[num].FR2 = (ID2_MASK << 3) | 0x04; 
    } else {
        CAN -> sFilterRegister[num].FR1 = (ID1 << 21); 
        CAN -> sFilterRegister[num].FR2 = (ID2_MASK << 21); 
    }

    CAN -> FA1R |= (1 << num);      //ACTIVE
    CAN -> FMR &= ~CAN_FMR_FINIT;
}

void CAN_Init(uint32_t speed, uint8_t mode){
    CAN -> MCR |= CAN_MCR_INRQ | CAN_MCR_ABOM;
    while (!(CAN -> MSR & CAN_MSR_INAK));
    CAN -> MCR &= ~CAN_MCR_SLEEP;
    CAN -> BTR = speed;
    if (mode & MODE_LISTEN_ONLY){
        CAN -> BTR |= CAN_BTR_SILM;
    }
    if (mode & MODE_LOOP_BACK){
        CAN -> BTR |= CAN_BTR_LBKM;
    }
    CAN -> MCR &= ~CAN_MCR_INRQ;
    while (CAN -> MSR & CAN_MSR_INAK);

    CAN -> IER = CAN_IER_FFIE0;
    NVIC_EnableIRQ(CEC_CAN_IRQn);
}

void CAN_DeInit(){
    CAN -> MCR |= CAN_MCR_INRQ;
    while (!(CAN -> MSR & CAN_MSR_INAK));
    CAN -> MCR |= CAN_MCR_SLEEP;
    CAN -> MCR &= ~CAN_MCR_INRQ;
    while (CAN -> MSR & CAN_MSR_INAK);
    NVIC_DisableIRQ(CEC_CAN_IRQn);
}

uint32_t CAN_GetSpeed(uint8_t i){
    switch (i){
        case '0': return CAN_SPEED_10K; break;
        case '1': return CAN_SPEED_20K; break;
        case '2': return CAN_SPEED_50K; break;
        case '3': return CAN_SPEED_100K; break;
        case '4': return CAN_SPEED_125K; break;
        case '5': return CAN_SPEED_250K; break;
        case '6': return CAN_SPEED_500K; break;
        case '7': return CAN_SPEED_800K; break;
        case '8': return CAN_SPEED_1M; break;
        default: return 0; break;
    }
}

void CAN_Transmit(_can_message message){
    uint8_t i;
    CAN -> sTxMailBox[0].TDTR = message.DLC;
    CAN -> sTxMailBox[0].TDLR = 0x00000000;
    for (i = 0; (i < message.DLC) && (i < 4); i++){
        CAN -> sTxMailBox[0].TDLR |= (message.data[i] << (i * 8));
    }
    CAN -> sTxMailBox[0].TDHR = 0x00000000;
    for (i = 4; (i < message.DLC) && (i < 8); i++){
        CAN -> sTxMailBox[0].TDHR |= (message.data[i] << ((i - 4) * 8));
    }
    if (message.type == CAN_STANDART){
        CAN -> sTxMailBox[0].TIR = (message.id << 21) | 0x01 | ((message.RTR) ? 0x02 : 0x00);
    } else {
        CAN -> sTxMailBox[0].TIR = (message.id << 3) | 0x05 | ((message.RTR) ? 0x02 : 0x00);
    }
    LED_TX_Blink();
}

uint8_t CAN_TxIsEmpty(){
    if ((CAN -> TSR & CAN_TSR_TME0) && (countTxBuf > 0)) return 0;
    return 1;
}

_can_message CAN_GetTxMessage(){
    _can_message message;
    if (countTxBuf > 0){
	message = can_tx_messages[headTxBuf];
	countTxBuf--;
	headTxBuf++;
	if (headTxBuf == CAN_TXBUF_SIZE) headTxBuf = 0;
    }
    return message;
}

void CAN_SetTxMessage(_can_message message){
    if (countTxBuf < CAN_TXBUF_SIZE){
        can_tx_messages[tailTxBuf] = message;
	tailTxBuf++;
	if (tailTxBuf == CAN_TXBUF_SIZE) tailTxBuf = 0;
	countTxBuf++;
    } else {
        LED_Error_Blink();
    }
}

uint8_t CAN_RxIsEmpty(){
    if (countRxBuf > 0) return 0;
    return 1;
}

_can_message CAN_GetRxMessage(){
    _can_message message;
    if (countRxBuf > 0){
	message = can_rx_messages[headRxBuf];
	countRxBuf--;
	headRxBuf++;
	if (headRxBuf == CAN_RXBUF_SIZE) headRxBuf = 0;
    }
    return message;
}

void CAN_SetRxMessage(_can_message message){
    if (countRxBuf < CAN_RXBUF_SIZE){
        can_rx_messages[tailRxBuf] = message;
	tailRxBuf++;
	if (tailRxBuf == CAN_RXBUF_SIZE) tailRxBuf = 0;
	countRxBuf++;
    } else {
        LED_Error_Blink();
    }
}
#pragma once

#include <stm32f0xx.h>
#include "..\LEDS\leds.h"

#define MODE_LOOP_BACK      2
#define MODE_LISTEN_ONLY    1
#define MODE_NORMAL         0

#define CAN_STANDART    0
#define CAN_EXTENDED    1

#define CAN_FILTER_MASK     0
#define CAN_FILTER_LIST     1

#define CAN_TXBUF_SIZE  10
#define CAN_RXBUF_SIZE  10

typedef struct {
    uint8_t type;
    uint32_t id;
    uint8_t DLC;
    uint8_t RTR;
    uint8_t data[8];
    uint16_t time;

    uint8_t source;
} _can_message;

void CAN_GPIO_Init();
void CAN_Init(uint32_t speed, uint8_t mode);
void CAN_DeInit();
uint32_t CAN_GetSpeed(uint8_t i);
void CAN_Transmit(_can_message message);
_can_message CAN_GetTxMessage();
void CAN_SetTxMessage(_can_message message);
_can_message CAN_GetRxMessage();
void CAN_SetRxMessage(_can_message message);
uint8_t CAN_TxIsEmpty();
uint8_t CAN_RxIsEmpty();
void CAN_SetFilter(uint8_t num, uint8_t filter_mode, uint8_t id_type, uint32_t ID1, uint32_t ID2_MASK);
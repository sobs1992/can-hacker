#include "leds.h"

#define LED4_ON     GPIOA -> BSRR = GPIO_BSRR_BR_2
#define LED4_OFF    GPIOA -> BSRR = GPIO_BSRR_BS_2
#define LED2_ON     GPIOA -> BSRR = GPIO_BSRR_BR_4
#define LED2_OFF    GPIOA -> BSRR = GPIO_BSRR_BS_4
#define LED3_ON     GPIOB -> BSRR = GPIO_BSRR_BR_1
#define LED3_OFF    GPIOB -> BSRR = GPIO_BSRR_BS_1
#define LED1_ON     GPIOB -> BSRR = GPIO_BSRR_BR_11
#define LED1_OFF    GPIOB -> BSRR = GPIO_BSRR_BS_11

uint8_t leds_connect, leds_tx, leds_rx, leds_error;

void LEDS_Connect_Handler(){
    static uint8_t flag = 0;
    if (flag == 0){
        LED1_ON;
    } else {
        LED1_OFF;
    }
    flag = !flag;
}

void LEDS_RX_Handler(){
    LED2_OFF;
}

void LEDS_TX_Handler(){
    LED3_OFF;
}

void LEDS_Error_Handler(){
    LED4_OFF;
}

void LED_Connect_On(){
    LED1_ON;
}

void LED_Connect_Off(){
    LED1_OFF;
}

void LED_Connect_Blink_On(){
    SuperTimer_En(leds_connect, T_ON);
}

void LED_Connect_Blink_Off(){
    SuperTimer_En(leds_connect, T_OFF);
    LED1_OFF;
}

void LED_RX_Blink(){
    LED2_ON;
    SuperTimer_En(leds_rx, T_ON);
}

void LED_TX_Blink(){
    LED3_ON;
    SuperTimer_En(leds_tx, T_ON);
}

void LED_Error_Blink(){
    LED4_ON;
    SuperTimer_En(leds_error, T_ON);
}

void Leds_Init(){
    RCC -> AHBENR |= RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN;
    GPIOA -> MODER |= GPIO_MODER_MODER2_0 | GPIO_MODER_MODER4_0;
    GPIOA -> OTYPER |= GPIO_OTYPER_OT_2 | GPIO_OTYPER_OT_4;
    GPIOA -> OSPEEDR |= GPIO_OSPEEDER_OSPEEDR2 | GPIO_OSPEEDER_OSPEEDR4;
    GPIOB -> MODER |= GPIO_MODER_MODER1_0 | GPIO_MODER_MODER11_0;
    GPIOB -> OTYPER |= GPIO_OTYPER_OT_1 | GPIO_OTYPER_OT_11;
    GPIOB -> OSPEEDR |= GPIO_OSPEEDER_OSPEEDR1 | GPIO_OSPEEDER_OSPEEDR11;
    SuperTimer_Init();
    
    leds_connect = SuperTimer_Create(T_MODE_NORMAL, 500, LEDS_Connect_Handler);
    leds_rx = SuperTimer_Create(T_MODE_OPM, 10, LEDS_RX_Handler);
    leds_tx = SuperTimer_Create(T_MODE_OPM, 10, LEDS_TX_Handler);
    leds_error = SuperTimer_Create(T_MODE_OPM, 500, LEDS_Error_Handler);

    LED1_OFF;
    LED2_OFF;
    LED3_OFF;
    LED4_OFF;
}
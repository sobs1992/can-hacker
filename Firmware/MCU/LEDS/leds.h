#pragma once

#include <stm32f0xx.h>
#include "..\SUPERTIMER\supertimer.h"

void Leds_Init();
void LED_RX_Blink();
void LED_TX_Blink();
void LED_Error_Blink();

void LED_Connect_On();
void LED_Connect_Off();
void LED_Connect_Blink_On();
void LED_Connect_Blink_Off();
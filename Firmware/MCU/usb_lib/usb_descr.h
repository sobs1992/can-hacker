#define DEVICE_DESCRIPTOR_SIZE_BYTE						18
#define CONFIG_DESCRIPTOR_SIZE_BYTE						67
#define DEVICE_QALIFIER_SIZE_BYTE						10
#define STRING_LANG_DESCRIPTOR_SIZE_BYTE                                        4
#define STRING_MANUFACTURING_DESCRIPTOR_SIZE_BYTE                               10
#define STRING_PRODUCT_DESCRIPTOR_SIZE_BYTE                                     22
#define STRING_SERIAL_DESCRIPTOR_SIZE_BYTE                                      16

//#define HID_REPORT_DESCRIPTOR_SIZE						52//25//63

typedef unsigned char uint8_t;

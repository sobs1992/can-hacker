//#include <stm32f0xx.h>
#include "usb_defs.h"
#include "usb_descr.h"

#define HID 

//������������ ��������� �������� �����
#define MAX_ENDPOINTS				8
//����������� �������
#define GET_DESCRIPTOR				0x06
#define SET_DESCRIPTOR				0x07	//�� ����������
#define SET_ADDRESS				0x05
#define SET_FEATURE				0x03	//�� ����������
#define CLEAR_FEATURE				0x01
#define GET_STATUS				0x00
#define GET_CONFIGURATION			0x08	//�� ����������
#define SET_CONFIGURATION			0x09
#define GET_INTERFACE				0x0A	//�� ����������
#define SET_INTERFACE				0x0B	//�� ����������
#define SYNC_FRAME				0x0C	//�� ����������
//��������� �������
#define DEVICE_DESCRIPTOR                       0x100
#define CONFIGURATION_DESCRIPTOR		0x200
#define HID_REPORT_DESCRIPTOR			0x2200
#define STRING_LANG_DESCRIPTOR			0x300
#define STRING_MAN_DESCRIPTOR			0x301
#define STRING_PROD_DESCRIPTOR			0x302
#define STRING_SN_DESCRIPTOR			0x303
#define DEVICE_QALIFIER_DESCRIPTOR		0x600

#define BULK_RESET				0xFF
#define GET_MAX_LUN				0xFE

// HID
#define SET_FEAUTRE				0x09
#define SET_IDLE_REQUEST			0x0A

// CDC
#define SET_LINE_CODING				0x20
#define GET_LINE_CODING                         0x21
#define SET_CONTROL_LINE_STATE                  0x22


#define CDC_PARITY_NONE                         0
#define CDC_PARITY_ODD                          1
#define CDC_PARITY_EVEN                         2
#define CDC_PARITY_MARK                         3
#define CDC_PARITY_SPACE                        4
#define CDC_STOP_1                              0
#define CDC_STOP_1_5                            1
#define CDC_STOP_2                              2

//������� ���������/������� ����� � EPnR ���������
#define CLEAR_DTOG_RX(R)       			(R & USB_EPnR_DTOG_RX) ? R : (R & (~USB_EPnR_DTOG_RX))
#define SET_DTOG_RX(R)         			(R & USB_EPnR_DTOG_RX) ? (R & (~USB_EPnR_DTOG_RX)) : (R | USB_EPnR_DTOG_RX)
#define TOGGLE_DTOG_RX(R)      			(R | USB_EPnR_DTOG_RX)
#define KEEP_DTOG_RX(R)        			(R & (~USB_EPnR_DTOG_RX))
#define CLEAR_DTOG_TX(R)       			(R & USB_EPnR_DTOG_TX) ? R : (R & (~USB_EPnR_DTOG_TX))
#define SET_DTOG_TX(R)         			(R & USB_EPnR_DTOG_TX) ? (R & (~USB_EPnR_DTOG_TX)) : (R | USB_EPnR_DTOG_TX)
#define TOGGLE_DTOG_TX(R)      			(R | USB_EPnR_DTOG_TX)
#define KEEP_DTOG_TX(R)        			(R & (~USB_EPnR_DTOG_TX))
#define SET_VALID_RX(R)        			((R & USB_EPnR_STAT_RX) ^ USB_EPnR_STAT_RX)   | (R & (~USB_EPnR_STAT_RX))
#define SET_NAK_RX(R)          			((R & USB_EPnR_STAT_RX) ^ USB_EPnR_STAT_RX_1) | (R & (~USB_EPnR_STAT_RX))
#define SET_STALL_RX(R)        			((R & USB_EPnR_STAT_RX) ^ USB_EPnR_STAT_RX_0) | (R & (~USB_EPnR_STAT_RX))
#define KEEP_STAT_RX(R)        			(R & (~USB_EPnR_STAT_RX))
#define SET_VALID_TX(R)        			((R & USB_EPnR_STAT_TX) ^ USB_EPnR_STAT_TX)   | (R & (~USB_EPnR_STAT_TX))
#define SET_NAK_TX(R)          			((R & USB_EPnR_STAT_TX) ^ USB_EPnR_STAT_TX_1) | (R & (~USB_EPnR_STAT_TX))
#define SET_STALL_TX(R)        			((R & USB_EPnR_STAT_TX) ^ USB_EPnR_STAT_TX_0) | (R & (~USB_EPnR_STAT_TX))
#define KEEP_STAT_TX(R)        			(R & (~USB_EPnR_STAT_TX))
#define CLEAR_CTR_RX(R)       		 	(R & (~(USB_EPnR_CTR_RX | USB_EPnR_STAT_RX | USB_EPnR_STAT_TX | USB_EPnR_DTOG_RX | USB_EPnR_DTOG_TX)))
#define CLEAR_CTR_TX(R)        			(R & (~(USB_EPnR_CTR_TX | USB_EPnR_STAT_RX | USB_EPnR_STAT_TX | USB_EPnR_DTOG_RX | USB_EPnR_DTOG_TX)))
#define CLEAR_CTR_RX_TX(R)     			(R & (~(USB_EPnR_CTR_TX | USB_EPnR_CTR_RX | USB_EPnR_STAT_RX | USB_EPnR_STAT_TX | USB_EPnR_DTOG_RX | USB_EPnR_DTOG_TX)))
//��������� ���������� USB
#define USB_DEFAULT_STATE				0
#define USB_ADRESSED_STATE				1
#define USB_CONFIGURE_STATE				2
#define USB_HID_READY                                   3
//���� �������� �����
#define EP_TYPE_BULK					0x00
#define EP_TYPE_CONTROL					0x01
#define EP_TYPE_ISO						0x02
#define EP_TYPE_INTERRUPT				0x03

extern uint8_t USB_DeviceDescriptor[];
extern uint8_t USB_ConfigDescriptor[];
extern uint8_t USB_StringLangDescriptor[];
extern uint8_t USB_StringManufacturingDescriptor[];
extern uint8_t USB_DeviceQualifierDescriptor[];
extern uint8_t USB_StringProdDescriptor[];
extern uint8_t HID_ReportDescriptor[];
extern uint8_t USB_StringSerialDescriptor[];

extern void EP_UserInit();

//��� ��� ����������� ����������������� ������
typedef struct {
	uint8_t bmRequestType;
	uint8_t bRequest;
	uint16_t wValue;
	uint16_t wIndex;
	uint16_t wLength;
} config_pack_t;
//��������� ��������� �������� �����
typedef struct {
	uint16_t *tx_buf;
	uint8_t *rx_buf;
	uint16_t status;
	unsigned rx_cnt : 10;
	unsigned tx_flag : 1;
	unsigned rx_flag : 1;
	unsigned setup_flag : 1;
} ep_t;
//������ � ����� ���������� USB
typedef struct {
	uint8_t USB_Status;
	uint16_t USB_Addr;
} usb_dev_t;

typedef struct {
    #pragma pack(push, 1)
    uint32_t dwDTERate;
    uint8_t bCharFormat;
    uint8_t bParityType;
    uint8_t bDataBits;
    #pragma pack(pop)
} usb_cdc_format_t;

extern ep_t endpoints[MAX_ENDPOINTS];

//������������� USB
void USB_Init();
//�������� ������ ���������� USB
uint8_t USB_GetState();
//������������� �������� �����
void EP_Init(uint8_t number, uint8_t type, uint16_t addr_tx, uint16_t addr_rx);
//������ ������� � ����� �������� ����� ����� ����������
void EP_Write(uint8_t number, uint8_t *buf, uint16_t size);
//������ ������� �� ������ �������� �����
void EP_Read(uint8_t number, uint8_t *buf);
//����������
void Enumerate(uint8_t number);
void EP_Write_NoBlock(uint8_t number, uint8_t *buf, uint16_t size);
void EP_SetStall(uint8_t number);
void EP_SetNAK_RX(uint8_t number);
void EP_SetNAK_TX(uint8_t number);
void EP_SetValid_RX(uint8_t number);
void EP_SetValid_TX(uint8_t number);
void USB_CDC_SetParameter(uint32_t baudrate, uint8_t data_bits, uint8_t parity, uint8_t stop);
void EP_Read_NoConfirm(uint8_t number, uint8_t *buf);
void EP_Confirm_Read(uint8_t number);
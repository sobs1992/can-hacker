#include "usb_descr.h"

const uint8_t USB_DeviceDescriptor[] = {
    0x12,	//bLength
    0x01,	//bDescriptorType
    0x10,	//bcdUSB_L
    0x01,	//bcdUSB_H
    0x02,	//bDeviceClass
    0x00,	//bDeviceSubClass
    0x00,	//bDeviceProtocol
    0x40,	//bMaxPacketSize
    0x83,	//idVendor_L
    0x04,	//idVendor_H
    0x40    ,	//idProduct_L
    0x57,	//idProduct_H
    0x01,	//bcdDevice_Ver_L
    0x00,	//bcdDevice_Ver_H
    0x01,	//iManufacturer
    0x02,	//iProduct
    0x03,	//iSerialNumber
    0x01	//bNumConfigurations
};

const uint8_t USB_DeviceQualifierDescriptor[] = {
    0x0A,	//bLength
    0x06,	//bDescriptorType
    0x00,	//bcdUSB_L
    0x02,	//bcdUSB_H
    0x02,	//bDeviceClass
    0x00,	//bDeviceSubClass
    0x00,	//bDeviceProtocol
    0x40,	//bMaxPacketSize0
    0x01,	//bNumConfigurations
    0x00	//Reserved
};

const uint8_t USB_ConfigDescriptor[] = {
//���������� ������������
    0x09,	//bLength
    0x02,	//bDescriptorType
    CONFIG_DESCRIPTOR_SIZE_BYTE,	//wTotalLength_L
    0x00,	//wTotalLength_H
    0x02,	//bNumInterfaces
    0x01,	//bConfigurationValue
    0x00,	//iConfiguration
    0x80,	//bmAttributes
    0x32,	//bMaxPower
//���������� ����������
    0x09,	//bLength
    0x04,	//bDescriptorType
    0x00,	//bInterfaceNumber
    0x00,	//bAlternateSetting
    0x01,	//bNumEndpoints
    0x02,	//bInterfaceClass(0x01 - Audio, 0x02 - CDC, 0x03 - HID, 0xFF - custom)
    0x02,	//bInterfaceSubClass
    0x00,	//bInterfaceProtocol
    0x00,	//iInterface
    /*Header Functional Descriptor*/
    0x05,   /* bLength: Endpoint Descriptor size */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x00,   /* bDescriptorSubtype: Header Func Desc */
    0x10,   /* bcdCDC: spec release number */
    0x01,
    /*Call Management Functional Descriptor*/
    0x05,   /* bFunctionLength */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x01,   /* bDescriptorSubtype: Call Management Func Desc */
    0x00,   /* bmCapabilities: D0+D1 */
    0x01,   /* bDataInterface: 1 */
    /*ACM Functional Descriptor*/
    0x04,   /* bFunctionLength */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x02,   /* bDescriptorSubtype: Abstract Control Management desc */
    0x02,   /* bmCapabilities */
    /*Union Functional Descriptor*/
    0x05,   /* bFunctionLength */
    0x24,   /* bDescriptorType: CS_INTERFACE */
    0x06,   /* bDescriptorSubtype: Union func desc */
    0x00,   /* bMasterInterface: Communication class interface */
    0x01,   /* bSlaveInterface0: Data Class Interface */
    //���������� �������� ����� 1 IN
    0x07,	//bLength
    0x05,	//bDescriptorType
    0x82,	//bEndpointAddress
    0x03,	//bmAttributes
    0x08,	//wMaxPacketSize_L
    0x00,	//wMaxPacketSize_H
    0x10,       //bInterval

//���������� ����������
    0x09,	//bLength
    0x04,	//bDescriptorType
    0x01,	//bInterfaceNumber
    0x00,	//bAlternateSetting
    0x02,	//bNumEndpoints
    0x0A,	//bInterfaceClass(0x01 - Audio, 0x0A - CDC, 0x03 - HID, 0xFF - custom)
    0x00,	//bInterfaceSubClass
    0x00,	//bInterfaceProtocol
    0x00,	//iInterface
    //���������� �������� ����� 1 IN
    0x07,	//bLength
    0x05,	//bDescriptorType
    0x01,	//bEndpointAddress
    0x02,	//bmAttributes
    0x40,	//wMaxPacketSize_L
    0x00,	//wMaxPacketSize_H
    0x00,         //bInterval
    //���������� �������� ����� 1 OUT
    0x07,	//bLength
    0x05,	//bDescriptorType
    0x81,	//bEndpointAddress
    0x02,	//bmAttributes
    0x40,	//wMaxPacketSize_L
    0x00,	//wMaxPacketSize_H
    0x00         //bInterval
};

const uint8_t USB_StringLangDescriptor[] = {
    0x04,	//bLength
    0x03,	//bDescriptorType
    0x09,	//wLANGID_L
    0x04	//wLANGID_H
};

const uint8_t USB_StringManufacturingDescriptor[] = {
    STRING_MANUFACTURING_DESCRIPTOR_SIZE_BYTE,		//bLength
    0x03,											//bDescriptorType
    'S', 0x00,										//bString...
    'O', 0x00,
    'B', 0x00,
    'S', 0x00
};

const uint8_t USB_StringProdDescriptor[] = {
    STRING_PRODUCT_DESCRIPTOR_SIZE_BYTE,		//bLength
    0x03,										//bDescriptorType
    'C', 0x00,									//bString...
    'A', 0x00,
    'N', 0x00,
    ' ', 0x00,
    'H', 0x00,
    'A', 0x00,
    'C', 0x00,
    'K', 0x00,
    'E', 0x00,
    'R', 0x00
};

const uint8_t USB_StringSerialDescriptor[STRING_SERIAL_DESCRIPTOR_SIZE_BYTE] =
{
    STRING_SERIAL_DESCRIPTOR_SIZE_BYTE,           /* bLength */
    0x03,        /* bDescriptorType */
    'R', 0,
    'H', 0,
    '-', 0,
    '0', 0,
    '0', 0,
    '0', 0,
    '1', 0
};

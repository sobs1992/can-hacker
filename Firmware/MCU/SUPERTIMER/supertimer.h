#ifndef SUPERTIMER_H
#define SUPERTIMER_H

#include <stm32f0xx.h>

#define T_MODE_NORMAL	0
#define T_MODE_OPM	1
#define T_ON		1
#define T_OFF		0

void delay_ms(uint16_t delay);
void SuperTimer_Init();
void SuperTimer_En(uint8_t id, uint8_t state);
void SuperTimer_Reset(uint8_t id);
uint8_t SuperTimer_Create(uint8_t mode, uint32_t period, void (*pFunc)());
uint32_t SuperTimer_GetTicks();
void SuperTimer_ChangePeriod(uint8_t id, uint32_t period);
uint8_t SuperTimer_GetCountTimer();
uint8_t SuperTimer_GetState(uint8_t id);

#endif
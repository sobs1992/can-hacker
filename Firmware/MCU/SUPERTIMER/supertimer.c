#include "supertimer.h"

#define MAX_TIMERS  16

typedef struct {
    unsigned id : 6;	    //max 64 timers
    unsigned mode : 1;
    unsigned en : 1;
    uint32_t period;
    uint32_t counter;
    void (*pFunc)();
} _timer;

static _timer timers[MAX_TIMERS];
static uint32_t delay_counter;
volatile uint32_t count_ticks;
static uint8_t count_timers;

uint32_t SuperTimer_GetTicks(){
    return count_ticks;
}

void SuperTimer_ChangePeriod(uint8_t id, uint32_t period){
    if (id == 0) return;
    if (id > MAX_TIMERS) return;
    timers[id - 1].counter = period;
    timers[id - 1].period = period;
}

uint8_t SuperTimer_Create(uint8_t mode, uint32_t period, void (*pFunc)()){
    uint8_t i;
    for (i = 0; i < MAX_TIMERS; i++){
	if (timers[i].id == 0){
	    timers[i].id = i + 1;
	    timers[i].mode = mode;
	    timers[i].en = 0;
	    timers[i].period = period;
            timers[i].counter = period;
	    timers[i].pFunc = pFunc;
            count_timers++;
	    break;
	}
    }
    if (i >= MAX_TIMERS) return 0xFF;
    return i + 1;
}

uint8_t SuperTimer_GetCountTimer(){
    return count_timers;
}

void SuperTimer_En(uint8_t id, uint8_t state){
    if (id == 0) return;
    if (id > MAX_TIMERS) return;
    timers[id - 1].en = state;
}

uint8_t SuperTimer_GetState(uint8_t id){
    if (id == 0) return T_OFF;
    if (id > MAX_TIMERS) return T_OFF;
    return timers[id - 1].en;
}

void SuperTimer_Reset(uint8_t id){
    if (id == 0) return;
    if (id > MAX_TIMERS) return;
    timers[id - 1].counter = timers[id - 1].period;
}

void SysTick_Handler(){
    uint8_t i;
    if (delay_counter > 0) delay_counter--;
    count_ticks++;
    for (i = 0; i < MAX_TIMERS; i++){
	if (timers[i].id == 0) break;
	if (timers[i].en == T_ON){
	    if (timers[i].counter > 0){
		timers[i].counter--;
	    } else {
		if (timers[i].mode == T_MODE_OPM){
		    timers[i].en = T_OFF;
		}
		timers[i].counter = timers[i].period;
		timers[i].pFunc();
	    }
	}
    }
}

void delay_ms(uint16_t delay){
    delay_counter = delay;
    while (delay_counter);
}

void SuperTimer_Init(){
    SystemCoreClockUpdate();
    SysTick_Config(SystemCoreClock / 1000);
    NVIC_SetPriority (SysTick_IRQn, 1);
    delay_counter = 0;
    count_ticks = 0;
    count_timers = 0;
}